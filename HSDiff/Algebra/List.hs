{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeFamilies #-}
module HSDiff.Algebra.List where

import HSDiff.Algebra

-- We define lists as the initial algebra
-- for the expected functor.
newtype L a x = L { unL :: Either () (a , x) }
  deriving (Eq, Ord)
pattern Nil       = L (Left ())
pattern Cons x xs = L (Right (x , xs))

instance (Show a , Show x) => Show (L a x) where
  show = show . unL

type List a = Fix (L a)

fromList :: [a] -> List a
fromList []     = Fix Nil
fromList (x:xs) = Fix (Cons x $ fromList xs)

toList :: List a -> [a]
toList (Fix Nil)         = []
toList (Fix (Cons x xs)) = x : toList xs

-- Our HasAlg class is not very similar to a type
-- having initial algebra, but instead allows us
-- to shallowly open and close it's constructors.
instance (Eq a, Show a) => HasAlg (L a) where
  ch (Fix Nil)         = []
  ch (Fix (Cons _ xs)) = [xs]
  
  hd (Fix Nil)        = Nil
  hd (Fix (Cons x _)) = Cons x ()
  
  close (Nil      , l )        = Just (Fix Nil , l)
  close (Cons x _ , (xs:rest)) = Just (Fix (Cons x xs) , rest)
  close x = Nothing
  
  str Nil         = "Nil"
  str (Cons x _)  = "Cons " ++ show x
  
instance HasSOP (L a x) where
  type SOP (L a x) = Either () (a , x)
  
  go = unL
  og = L
  
  typeStr _           = "L"
