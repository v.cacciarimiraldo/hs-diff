{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module HSDiff.TH.GenSOP where

import HSDiff.Algebra
import Language.Haskell.TH
import Language.Haskell.TH.Syntax
import Control.Monad (replicateM)

--------------------------------------
--  Generation of HasIso instances
--

-- Generates n pattern and exp new variables.
genPE :: Int -> Q ([PatQ], [ExpQ])
genPE n = do
  ids <- replicateM n (newName "v")
  return (map varP ids, map varE ids)

-- Given a list of types, generates a right-assoc'ed coproduct. 
mkEither :: [TypeQ] -> TypeQ
mkEither []      = tupleT 0
mkEither [x]     = x
mkEither [x , y] = appT (appT (conT (mkName "Either")) x) y
mkEither (t:ts)  = appT (appT (conT (mkName "Either")) t) $ mkEither ts

-- TODO: check n >= m, invariant!
mkInjExp :: Int -> Int -> ExpQ -> ExpQ
mkInjExp n m exp = choose (m - 1) $ last exp
  where
    last exp
      | m == n    = exp
      | otherwise = appE (conE (mkName "Left")) exp
      
    choose 0 exp = exp
    choose n exp = appE (conE (mkName "Right")) (choose (n-1) exp)

-- Generate the proper patterns                                
mkInjPat :: Int -> Int -> PatQ -> PatQ
mkInjPat n m pat = choose (m - 1) $ last pat
  where
    last pat
      | m == n    = pat
      | otherwise = conP (mkName "Left") [pat]
      
    choose 0 pat = pat
    choose n pat = conP (mkName "Right") [choose (n-1) pat]

mkTupExp :: [ExpQ] -> ExpQ 
mkTupExp [] = [| () |]
mkTupExp [x]    = [| $x |]
mkTupExp (v:vs) = [| ($v , $(mkTupExp vs)) |]

mkTupPat :: [PatQ] -> PatQ
mkTupPat []      = tupP []
mkTupPat [x]     = x
mkTupPat [x , y] = tupP [x , y]
mkTupPat (v:vs)  = tupP [v , mkTupPat vs]

mkTupType :: [TypeQ] -> TypeQ
mkTupType []      = tupleT 0
mkTupType [x]     = x
mkTupType [x , y] = appT (appT (tupleT 2) x) y
mkTupType (v:vs)  = appT (appT (tupleT 2) v) (mkTupType vs)

-- For a given constructor, indexed as i, generates the
-- important "go" and "og" functions together with the
-- type it is isomorphic to.
makeSOPClause :: Int -> Int -> Con -> Q (ClauseQ , ClauseQ , TypeQ)
makeSOPClause max i (NormalC name fields)
  = do
    (ps , vs) <- genPE (length fields)
    let goClause = clause [conP name ps] 
                          (normalB [| $(mkInjExp max i $ mkTupExp vs) |]) []
    let ogClause = clause [mkInjPat max i $ mkTupPat ps]   
                          (normalB (appsE (conE name : vs))) []
    let ty       = mkTupType (map (return . snd) fields)
    return (goClause, ogClause, ty)
    
genSOP :: Name -> Q [Dec]
genSOP t
  = do
    TyConI (DataD _ _ _ cons _) <- reify t
    deriveSOPAux t cons
    
makeTypeStrClause :: Name -> DecQ
makeTypeStrClause t
  = funD (mkName "typeStr") 
      [clause [wildP] (normalB (litE $ stringL (nameBase t))) []]
    
deriveSOPAux :: Name -> [Con] -> Q [Dec]
deriveSOPAux t cons
  = do
    let idxCons = zip [1..] cons
    (gos , ogs , tys) <- 
      unzip3 <$> mapM (uncurry (makeSOPClause (length cons))) idxCons
    finalT <- appT (conT $ mkName "HasSOP") (conT t)
    toT    <- (mkEither tys) >>= deriveSOPfam t
    goF    <- funD (mkName "go") gos
    ogF    <- funD (mkName "og") ogs
    typeStrF <- makeTypeStrClause t
    return [InstanceD [] finalT [toT , goF , ogF, typeStrF]]
    
deriveSOPfam :: Name -> Type -> Q Dec
deriveSOPfam newty ty
  = return (TySynInstD (mkName "SOP") (TySynEqn [ConT newty] ty))
