module HSDiff.Util.MemoizedMod (memoized , Cached, Memo) where

-- code from: https://gist.github.com/ramntry/345139e76c3aec92e78e

import Control.Monad.State
import qualified Data.Map as M

type Memo a v = State (M.Map a v)

recall :: (Ord a) => a -> Memo a v (Maybe v)
recall ha = M.lookup ha <$> get

llacer :: (Ord a) => a -> v -> Memo a v ()
llacer ha v = modify (M.insert ha v)


type Cached idx a v 
  = (idx -> a -> Memo idx v v) -> (idx -> a -> Memo idx v v)

closeCache :: (Ord idx) 
           => Cached idx a v -> idx -> a -> Memo idx v v
closeCache cached idx a
  = do
    r <- recall idx
    case r of
      Just result -> return result
      Nothing -> do
        result <- cached (closeCache cached) idx a
        llacer idx result
        return result
        
memoized :: (Ord idx) => Cached idx a v -> idx -> a -> v
memoized cached i a = evalState (closeCache cached i a) M.empty
