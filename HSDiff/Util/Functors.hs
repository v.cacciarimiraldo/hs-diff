{-# LANGUAGE TypeOperators        #-}
module HSDiff.Util.Functors where

data Empty

instance Show Empty where
  show _ = error "How come you produced this?"

data Id a
  = Id a
  deriving (Eq, Show)

data K a r 
  = K a 
  deriving (Eq, Show)
  
data (f :*: g) r 
  = f r :*: g r
  deriving (Eq, Show)
  
data (f :+: g) r
  = L (f r) | R (g r)
  deriving (Eq, Show)
  
