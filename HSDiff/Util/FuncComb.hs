module HSDiff.Util.FuncComb where

infix 5  ><
infix 4  -|-

-- (1) Product -----------------------------------------------------------------

split :: (a -> b) -> (a -> c) -> a -> (b,c)
split f g x = (f x, g x)

(><) :: (a -> b) -> (c -> d) -> (a,c) -> (b,d)
f >< g = split (f . p1) (g . p2)

-- the 0-adic split 

(!), bang :: a -> ()
(!) = const ()
bang = (!)

-- Renamings:

p1 :: (a, b) -> a
p1 = fst

p2 :: (a, b) -> b
p2 = snd

-- utils

swap :: (a , b) -> (b , a)
swap = split p2 p1

pair :: a -> b -> (a , b)
pair a b = (a , b)

-- (2) Coproduct ---------------------------------------------------------------

-- Renamings:

i1 :: a -> Either a b
i1 = Left

i2 :: b -> Either a b 
i2 = Right

i11 :: a -> Either (Either a b) (Either c d)
i11 = i1 . i1

i12 :: b -> Either (Either a b) (Either c d) 
i12 = i1 . i2

i21 :: c -> Either (Either a b) (Either c d) 
i21 = i2 . i1

i22 :: d -> Either (Either a b) (Either c d)
i22 = i2 . i2

-- either is predefined

(-|-) :: (a -> b) -> (c -> d) -> Either a c -> Either b d
f -|- g = either (i1 . f) (i2 . g)

ite :: Bool -> x -> x -> x
ite i t e = if i then t else e
