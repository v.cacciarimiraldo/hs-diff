{-# OPTIONS_GHC -cpp              #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE PolyKinds            #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE Rank2Types #-}
module HSDiff.Diffable.Base where

import Data.Proxy
#define P Proxy :: Proxy
import Data.Void
import Unsafe.Coerce

import HSDiff.Diffable.Size
import HSDiff.Algebra
import HSDiff.Util.FuncComb

-- |Unlike our Agda model, the D type in Haskell
--  will be handled as a single GADT, instead
--  of separating the fixpoint diff.
--
--  The reason for this is the fact that it is much easier
--  to deffine generic operations over D, only, than
--  over a family of GADTs.  
data D :: (* -> *) -> * -> * where
  -- Conflicts
  DA   :: r a                     -> D r a
  DmuA :: (HasAlg a) 
       => r (Fix a) -> D r (Fix a) -> D r (Fix a)

  -- Tags
  DTag :: (HasSOP a , Diffable (SOP a)) 
       => D r (SOP a) -> D r a
  
  -- Products
  DPair :: (Diffable a , Diffable b)
        => D r a -> D r b -> D r (a, b)

  -- Coproducts
  DInL  :: (Diffable a , Diffable b)
        => D r a -> D r (Either a b)
  DInR  :: (Diffable a , Diffable b)
        => D r b -> D r (Either a b)
  DSetR :: b -> a -> D r (Either a b)
  DSetL :: a -> b -> D r (Either a b)
  
  -- Fixpoints
  DmuEnd :: D r (Fix a)
  
  DmuIns :: (HasAlg a) 
         => a () -> D r (Fix a) -> D r (Fix a)
  DmuDel :: (HasAlg a) 
         => a () -> D r (Fix a) -> D r (Fix a)
  DmuCpy :: (HasAlg a) 
         => a () -> D r (Fix a) -> D r (Fix a)
  DmuDwn :: (HasAlg a, Diffable (a ())) 
         => D r (a ()) -> D r (Fix a) -> D r (Fix a)
  
  -- Atoms
  D0Cpy    :: D r (Atom a)
  D0Set    :: (Eq a) => a -> a -> D r (Atom a)

  -- Unit
  D1 :: D r ()
    
-- The user indicates which parts of his AST are atomic
-- by wrapping it with an Atom constructor.
newtype Atom a = Atom { unAtom :: a }
  deriving (Eq, Ord)
  
instance (Show a) => Show (Atom a) where
  show = show . unAtom
  
instance Sized (Atom a) where
  size _ = 1
  
-- Simple error function.
patchCErr :: String -> a
patchCErr loc
  = error $ loc ++ ": received conflicting patches."
    
------------------------
-- Diffable typeclass --
------------------------

data Void2 a 

data Conflict :: * -> * where
  UpdUpd :: a -> a -> a  -> Conflict a
  UpdDel :: (HasAlg a)
         => a () -> a () -> Conflict (Fix a)
  DelUpd :: (HasAlg a)
         => a () -> a () -> Conflict (Fix a)
  GrowL  :: (HasAlg a)
         => a ()         -> Conflict (Fix a)
  GrowR  :: (HasAlg a)
         => a ()         -> Conflict (Fix a)
  GrowLR :: (HasAlg a)
         => a () -> a () -> Conflict (Fix a)

deriving instance (Eq a) => Eq (Conflict a)

type Patch  x = D Void2 x
type PatchC x = D Conflict x
  
-- b :>: a encodes the notion of "a is a subtype of b",
-- letting us navitage in a value of type b until we reach a,
-- in a type-safe manner.
data (:>:) :: * -> * -> * where
  STrefl  :: a :>: a
  STleft  :: b :>: a -> Either b c :>: a
  STright :: c :>: a -> Either b c :>: a
  STpi1   :: b :>: a -> (b , c) :>: a
  STpi2   :: c :>: a -> (b , c) :>: a
  STtag   :: (HasSOP b) => SOP b :>: a -> b :>: a
  STfix   :: (HasAlg b) => b () :>: a -> Fix b :>: a
  
-- Furthermore, this type can be shown and compared.
deriving instance Show (a :>: b)
deriving instance Eq   (a :>: b)
  
-- Now we can have a species of dependent sum here.
-- This is usefull for speaking about conflicts
-- without loosing type-safety.
data Ex :: (* -> *) -> * -> * where
  Ex :: (a :>: x) -> r x -> Ex r a
  
-- Unfortunately, we can't have a very expressive show
-- instance. We can always show the location
-- of an Ex, though.
instance Show (Ex r a) where
  show (Ex prf rx) = show prf
  
exmap :: (forall x . a :>: x -> b :>: x) -> Ex r a -> Ex r b
exmap f (Ex prf rx) = Ex (f prf) rx

cast :: D Void2 a -> D r a
cast = unsafeCoerce

class (Sized a) => Diffable a where 
  wdiff :: a -> a -> (Patch a , Int)
  
  diff :: a -> a -> Patch a
  diff x y = fst (wdiff x y)
  
  apply :: Patch a -> a -> Maybe a
  
  res :: Patch a -> Patch a -> Maybe (PatchC a)
  
  cost :: Patch a -> Int
  
-- |We can always compute the identity diff
diffID :: (Diffable a) => a -> D Void2 a
diffID x = diff x x

------------------------
-- Some functionality --
------------------------

dmuGlue :: D r (Fix a) -> D r (Fix a) -> D r (Fix a)
dmuGlue DmuEnd       r = r
dmuGlue (DmuIns x d) r = DmuIns x (dmuGlue d r)
dmuGlue (DmuDel x d) r = DmuDel x (dmuGlue d r)
dmuGlue (DmuCpy x d) r = DmuCpy x (dmuGlue d r)
dmuGlue (DmuDwn x d) r = DmuDwn x (dmuGlue d r)
dmuGlue (DmuA ra d)  r = DmuA ra (dmuGlue d r)
  
---------------------------
-- And trivial instances --
---------------------------

instance Diffable () where 
  cost _ = 0
  wdiff _ _ = (D1 , 0)
  apply _ _ = Just ()
  res _ _ = Just D1
  
instance (Eq a) => Diffable (Atom a) where  
  cost D0Cpy = 0
  cost _     = 1

  wdiff (Atom x) (Atom y)
    | x == y    = (D0Cpy , 0)
    | otherwise = (D0Set x y , 1)
    
  apply D0Cpy x = Just x
  apply (D0Set m n) (Atom x)
    | m == x    = Just (Atom n)
    | otherwise = Nothing
  apply _ x = patchCErr "(apply :: Atom a)"
    
  res D0Cpy D0Cpy       = Just D0Cpy
  res D0Cpy (D0Set x y) = Just D0Cpy -- $ D0Set x y
  res (D0Set x y) D0Cpy = Just $ D0Set x y
  res (D0Set x y) (D0Set w z)
    | x == w = Just $ 
        if y == z 
          then D0Set x y
          else DA (UpdUpd (Atom x) (Atom y) (Atom z))
    | otherwise = Nothing
  res _ _
    = patchCErr "(res :: Atom a)"

