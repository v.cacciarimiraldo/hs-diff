{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE ScopedTypeVariables  #-}
module HSDiff.Diffable.Instances.Coproduct where


import HSDiff.Util.FuncComb ((><))
import HSDiff.Diffable.Base
import HSDiff.Diffable.Size

instance (Eq a , Eq b, Diffable a , Diffable b) => Diffable (Either a b) where
  cost (DInL p) = 1 + cost p
  cost (DInR p) = 1 + cost p
  cost (DSetL xa xb) = 2 * (size xa + size xb)
  cost (DSetR xa xb) = 2 * (size xa + size xb)

  wdiff (Left a1)  (Left a2)  = DInL >< (+1) $ wdiff a1 a2
  wdiff (Right b1) (Right b2) = DInR >< (+1) $ wdiff b1 b2
  wdiff (Left a)   (Right b)  = (DSetL a b , 2 * (size a + size b))
  wdiff (Right b)  (Left a)   = (DSetR b a , 2 * (size a + size b))
  
  apply (DInL pa)     (Left a)  = Left  <$> apply pa a
  apply (DInR pb)     (Right b) = Right <$> apply pb b
  apply (DSetL xa xb) (Left a)
    | xa == a   = Just (Right xb)
    | otherwise = Nothing
  apply (DSetR xb xa) (Right b)
    | xb == b   = Just (Left xa)
    | otherwise = Nothing
  apply (DSetL _ _) (Right _) = Nothing
  apply (DSetR _ _) (Left _)  = Nothing
  apply (DInL _)    (Right _) = Nothing
  apply (DInR _)    (Left _)  = Nothing
  apply _ _
    = patchCErr "(apply :: Either a b)"
  
  res (DInL pa) (DInL pb) = DInL <$> res pa pb
  res (DInR pa) (DInR pb) = DInR <$> res pa pb
  res (DSetL xa xb) (DSetL ya yb)
    | xa /= ya  = Nothing
    | otherwise = Just $ 
      if (xb == yb)
        then DSetL xa xb
        else DA $ UpdUpd (Left xa) (Right xb) (Right yb)
  res (DSetR xa xb) (DSetR ya yb)
    | xa /= ya  = Nothing
    | otherwise = Just $ 
      if (xb == yb)
        then DSetR xa xb
        else DA $ UpdUpd (Right xa) (Left xb) (Left yb) 
  res (DSetL xa xb) (DInL pa) =
    case apply pa xa of
      Nothing  -> Nothing
      Just xa' -> Just $ if xa == xa'
                  then DSetL xa xb
                  else DA (UpdUpd (Right xb) (Left xa) (Left xa'))
                  -- else Nothing
  res (DSetR xa xb) (DInR pa) =
    case apply pa xa of
      Nothing  -> Nothing
      Just xa' -> Just $ if xa == xa'
                  then DSetR xa xb
                  else DA (UpdUpd (Left xb) (Right xa) (Right xa'))
        {- ...
                  then Just (DSetR xa xb)
                  else Nothing
        -}
  res (DInL pa) (DSetL xa xb) =
    case apply pa xa of
      Nothing  -> Nothing
      Just xa' -> Just $ if xa == xa'
                  then DSetL xa xb
                  else DA (UpdUpd (Left xa') (Left xa) (Right xb))
        {- Exp 2: OLD
                  then Just (DSetL xa xb)
                  else Nothing
        -}
  res (DInR pa) (DSetR xa xb) =
    case apply pa xa of
      Nothing  -> Nothing
      Just xa' -> Just $ if xa == xa'
                  then DSetR xa xb
                  else DA (UpdUpd (Right xa') (Right xa) (Left xb))
          {- Exp3 : OLD
                  then Just (DSetR xa xb)
                  else Nothing
          -}

  res x y = patchCErr "(res :: Either a b)"
