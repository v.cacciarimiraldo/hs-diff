{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module HSDiff.Diffable.Instances.Product where

import HSDiff.Util.FuncComb ((><), pair)
import HSDiff.Diffable.Base

instance (Diffable a, Diffable b) => Diffable (a , b) where 
  cost (DPair pa pb) = cost pa + cost pb
 
  wdiff (a1 , b1) (a2 , b2)
    = let (r1 , c1) = wdiff a1 a2
      in (DPair r1) >< (c1 +) $ wdiff b1 b2
  
  apply (DPair pa pb) (x , y)
    = pair <$> apply pa x <*> apply pb y
    
  res (DPair pa pb) (DPair qa qb)
    = DPair <$> res pa qa <*> res pb qb

