{-# OPTIONS_GHC -cpp              #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE BangPatterns         #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE UndecidableInstances #-}
module HSDiff.Diffable.Instances.FixpointMatrix where

import Data.Proxy
#define P Proxy :: Proxy
import Math.LinearAlgebra.Sparse.Matrix

import HSDiff.Util.FuncComb ((><), pair)
import HSDiff.Diffable.Base
import HSDiff.Algebra
import HSDiff.Util.MemoizedMod

import Data.List (partition)

import Control.Monad
import Control.Monad.State

----------------------------------------------
-- General functionality

data DiffInfo a
  = DI { src :: [a ()]
       , tgt :: [a ()]
       , matrix :: SparseMatrix Int
       , pR :: SparseMatrix Int
       , pC :: SparseMatrix Int
       }
       
diffInfo :: (HasAlg a) => Fix a -> Fix a -> DiffInfo a
diffInfo x y 
  = let
    lx = serialize x
    ly = serialize y
    rows = length lx
    cols = length ly
  in DI lx ly 
    (populateMx (idxed lx) (idxed ly) $ zeroMx (rows , cols)) 
    emptyMx 
    emptyMx
  where
    idxed = zip [1..]
    
    populateMx [] _ m = m
    populateMx _ [] m = m
    populateMx ((i , a):as) bs m
      = let (isa , nisa) = partition ((== a) . snd) bs
        in flag1s i (map fst isa) $ populateMx as nisa m
        
    flag1s i js m 
      = foldl (\a j -> ins a ((i , j) , 1)) m js

