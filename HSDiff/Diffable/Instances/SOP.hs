{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE UndecidableInstances #-}
module HSDiff.Diffable.Instances.SOP where

import HSDiff.Algebra
import HSDiff.Diffable.Base
import HSDiff.Util.FuncComb ((><))

import Unsafe.Coerce

instance {-# OVERLAPPABLE #-} 
    (HasSOP a , Diffable (SOP a)) 
    => Diffable a where 
  cost (DTag dxy) = cost dxy
    
  wdiff x y
    = let (dxy , c) = wdiff (go x) (go y)
      in (DTag dxy , c)
  
  apply (DTag dx) x
    = og <$> apply dx (go x)
    
  res (DTag dx) (DTag dy)
    = DTag <$> res dx dy
  
