{-# OPTIONS_GHC -cpp              #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE BangPatterns         #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE KindSignatures       #-}
{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE PolyKinds            #-}
module HSDiff.Diffable.Instances.Fixpoint where

import Data.Proxy
#define P Proxy :: Proxy

import HSDiff.Util.FuncComb ((><), pair)
import HSDiff.Diffable.Base
import HSDiff.Diffable.Size

import HSDiff.Algebra
import HSDiff.Util.MemoizedMod

type DMu r a = D r (Fix a)

instance (HasAlg (a :: * -> *) , Diffable (a ())) 
    => Diffable (Fix a) where
  cost = gcostmu (P a)
  
  -- Note that we need to unroll x and y BEFORE start memoizing,
  -- otherwise repeating lengths will occur.
  wdiff x y = wdiffL (serialize x) (serialize y)
  
  apply d x = gapplyL d [x] >>= assertSingleton
    where
      assertSingleton :: [x] -> Maybe x
      assertSingleton (x:_) = Just x
      assertSingleton _   = Nothing
      
  res = gresL (P (Fix a))

gcostmu :: (HasAlg (a :: * -> *) , Sized (a ())) 
        => Proxy a -> DMu Void2 a -> Int
gcostmu _ (DmuEnd)     = 0
gcostmu p (DmuDwn x d) = cost x + gcostmu p d
gcostmu p (DmuCpy x d) = gcostmu p d
gcostmu p (DmuIns x d) = size x + gcostmu p d
gcostmu p (DmuDel x d) = size x + gcostmu p d
gcostmu _ _            = patchCErr "gcostmu"

gcostmuS :: (HasAlg (a :: * -> *) , Sized (a ())) 
        => Proxy a -> DMu Void2 a -> Int
gcostmuS _ (DmuEnd)     = 0
gcostmuS p (DmuDwn x d) = cost x
gcostmuS p (DmuCpy x d) = 0
gcostmuS p (DmuIns x d) = size x
gcostmuS p (DmuDel x d) = size x
gcostmuS _ _            = patchCErr "gcostmuS"

wdiffL :: (HasAlg a, Diffable (a ()) , Sized (a ())) 
        => [a ()] -> [a ()] -> (DMu Void2 a , Int)
wdiffL la lb 
  = memoized memoDiff (length la , length lb) (la , lb)
  where     
    lub :: (x , Int) -> (x , Int) -> (x , Int)
    lub (x1 , c1) (x2 , c2)
      | c1 <= c2  = (x1 , c1)
      | otherwise = (x2 , c2) 
      
    add :: (HasAlg a, Diffable (a ()))
        => (DMu Void2 a -> DMu Void2 a) -> (DMu Void2 a , Int) 
        -> (DMu Void2 a , Int)
    add f (d , c) = (f d , gcostmuS (P a) (f d) + c)
  
    memoDiff :: (HasAlg a, Diffable (a ()))
             => Cached (Int , Int) ([a ()] , [a ()]) (DMu Void2 a , Int)
             
    memoDiff self (_,_) ([] , []) = return (DmuEnd , 0)
    
    memoDiff self (!m, _) (x:xs , [])
      = add (DmuDel x) 
        <$> self (m - 1 , 0) (xs , []) 
    memoDiff self (_,!n) ([] , y:ys)
      = add (DmuIns y)
        <$> self (0 , n - 1) ([] , ys)  
    memoDiff self (!m,!n) (x:xs , y:ys)
      | x == y
        = add (DmuCpy x)
          <$> self (m - 1 , n - 1) (xs , ys)
      | otherwise
        = do
          d1 <- add (DmuIns y) 
                  <$> self (m , n - 1) (x:xs , ys)
          d2 <- add (DmuDel x)
                  <$> self (m - 1 , n) (xs , y:ys)
          let (dxy , cxy) = wdiff x y
          d3 <- (id >< (+cxy)) . add (DmuDwn dxy)
                  <$> self (m - 1, n - 1) (xs , ys)
          return $ lub d1 (lub d2 d3)

gIns :: (Diffable (a ()) , HasAlg a)
     => a () -> [Fix a] -> Maybe [Fix a]
gIns v l = uncurry (:) <$> close (v , l)

gDel :: (Diffable (a ()) , HasAlg a)
     => a () -> [Fix a] -> Maybe [Fix a]
gDel _ [] = Nothing
gDel v (a:as)
  | hd a == v = Just (ch a ++ as)
  | otherwise = Nothing
          
gapplyL :: (Diffable (a ()) , HasAlg a)
        => DMu Void2 a -> [Fix a] -> Maybe [Fix a]
gapplyL DmuEnd l = Just l
-- gapplyL DmuEnd _  = Nothing
gapplyL (DmuIns x d) l 
  = gapplyL d l >>= gIns x
gapplyL (DmuCpy x d) l
  = gDel x l >>= gapplyL d >>= gIns x
gapplyL (DmuDel x d) l
  = gDel x l >>= gapplyL d
gapplyL (DmuDwn _ _) [] = Nothing
gapplyL (DmuDwn x d) (l : ls)
  = case apply x (hd l) of
        Nothing   -> Nothing
        Just hdL' -> gapplyL d (ch l ++ ls) >>= gIns hdL' 
gapplyL _ _ = patchCErr "gapplyL"


gresL :: (HasAlg a , Diffable (a ())) 
      => Proxy (Fix a) -> DMu Void2 a -> DMu Void2 a -> Maybe (DMu Conflict a)
gresL _ DmuEnd DmuEnd = Just DmuEnd
gresL p (DmuIns x xs) (DmuIns y ys)
 | x == y    = (DmuCpy x)   <$> gresL p xs ys
 | otherwise = (DmuA $ GrowLR x y) <$> gresL p xs ys
gresL p (DmuIns x xs) ys 
  -- OLD
  = (DmuA $ GrowL x) <$> gresL p xs ys
  -- = DmuIns x <$> gresL p xs ys
gresL p xs (DmuIns y ys) 
  -- OLD
  = (DmuA $ GrowR y) <$> gresL p xs ys
  -- = DmuCpy y <$> gresL p xs ys
gresL p (DmuCpy x xs) (DmuCpy y ys)
  | x == y    = (DmuCpy x) <$> gresL p xs ys
  | otherwise = Nothing
gresL p (DmuDel x xs) (DmuCpy y ys)
  | x == y    = (DmuDel x) <$> gresL p xs ys
  | otherwise = Nothing
gresL p (DmuCpy x xs) (DmuDel y ys)
  | x == y    = gresL p xs ys
  | otherwise = Nothing
gresL p (DmuDel x xs) (DmuDel y ys)
  | x == y    = gresL p xs ys
  | otherwise = Nothing
gresL p (DmuDwn x xs) (DmuCpy y ys)
  | Just _ <- apply x y -- = (DmuDwn (cast x)) <$> gresL p xs ys
      = DmuDwn <$> res x (diffID y) <*> gresL p xs ys
  | otherwise            = Nothing
gresL p (DmuCpy x xs) (DmuDwn y ys)
  | Just z <- apply y x -- = (DmuCpy z) <$> gresL p xs ys
      = DmuDwn <$> res (diffID x) y <*> gresL p xs ys
  | otherwise            = Nothing
gresL p (DmuDwn x xs) (DmuDwn y ys)
  = DmuDwn <$> res x y <*> gresL p xs ys
gresL p (DmuDwn x xs) (DmuDel y ys)
  | Just z <- apply x y = (DmuA $ UpdDel z y) <$> gresL p xs ys
  | otherwise            = Nothing
gresL p (DmuDel x xs) (DmuDwn y ys)
  | Just z <- apply y x = (DmuA $ DelUpd x z) <$> gresL p xs ys
  | otherwise            = Nothing
gresL _ _ _ = patchCErr "gresL"

