{-# OPTIONS_GHC -cpp              #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE PolyKinds            #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE BangPatterns         #-}
{-# LANGUAGE Rank2Types           #-}
module HSDiff.Diffable.Traversals where

import Control.Monad
import Control.Monad.Reader
import Control.Monad.Identity

import HSDiff.Diffable

import Debug.Trace

-- |A monadic traversal through a patch, changing
--  every (r a) for (s a).
walkM :: (Monad m) => (forall a . r a -> m (s a)) -> D r a -> m (D s a)
walkM f D1          = return D1
walkM f D0Cpy       = return D0Cpy
walkM f (D0Set x y) = return (D0Set x y)
walkM f (DInL da)   = DInL <$> walkM f da
walkM f (DInR db)   = DInR <$> walkM f db
walkM f (DSetL x y) = return (DSetL x y)
walkM f (DSetR x y) = return (DSetR x y)
walkM f (DTag d)    = DTag <$> walkM f d
walkM f (DPair da db) = DPair <$> walkM f da <*> walkM f db
walkM f (DmuIns x d) = DmuIns x <$> walkM f d
walkM f (DmuDel x d) = DmuDel x <$> walkM f d
walkM f (DmuCpy x d) = DmuCpy x <$> walkM f d
walkM f (DmuDwn x d) = DmuDwn <$> walkM f x <*> walkM f d
walkM f DmuEnd       = return DmuEnd
walkM f (DmuA dra d) = DmuA <$> f dra <*> walkM f d
walkM f (DA dra)     = DA <$> f dra

-- |And a non-monadic version
walk :: (forall a . r a -> s a) -> D r a -> D s a
walk f = runIdentity . walkM (return . f)

-- | Partial conflict solver.
solveP :: (forall a . r a -> Maybe (Patch a)) -> D r a -> D r a
solveP f D1          = D1
solveP f D0Cpy       = D0Cpy
solveP f (D0Set x y) = D0Set x y
solveP f (DInL da)   = DInL (solveP f da)
solveP f (DInR db)   = DInR (solveP f db)
solveP f (DSetL x y) = DSetL x y
solveP f (DSetR y x) = DSetR y x
solveP f (DTag da)   = DTag (solveP f da)
solveP f (DPair da db) = DPair (solveP f da) (solveP f db)
solveP f (DmuIns x d) = DmuIns x (solveP f d)
solveP f (DmuCpy x d) = DmuCpy x (solveP f d)
solveP f (DmuDel x d) = DmuDel x (solveP f d)
solveP f (DmuDwn x d) = DmuDwn (solveP f x) (solveP f d) 
solveP f DmuEnd       = DmuEnd
solveP f (DmuA x d)
  = case f x of
    Nothing -> DmuA x (solveP f d)
    Just r  -> dmuGlue (cast r) (solveP f d)
solveP f (DA ra)
  = case f ra of
      Nothing -> (DA ra)
      Just r  -> cast r

solveLocM :: (Monad m) 
          => (a :>: b) 
          -> (r b -> m (Maybe (Patch b))) 
          -> D r a 
          -> m (D r a)
solveLocM STrefl f (DA ra)
  = f ra >>= return . maybe (DA ra) cast
solveLocM STrefl f (DmuA ra d)
  = do
    rec <- solveLocM STrefl f d
    f ra >>= return . maybe (DmuA ra rec) (flip dmuGlue rec . cast)
solveLocM STrefl f (DmuIns x d) 
  = DmuIns x <$> solveLocM STrefl f d
solveLocM STrefl f (DmuDel x d)
  = DmuDel x <$> solveLocM STrefl f d
solveLocM STrefl f (DmuCpy x d)
  = DmuCpy x <$> solveLocM STrefl f d
solveLocM STrefl f (DmuDwn x d)
  = DmuDwn x <$> solveLocM STrefl f d
solveLocM (STtag prf) f (DTag da)
  = DTag <$> solveLocM prf f da
solveLocM (STpi1 prf) f (DPair da db)
  = flip DPair db <$> solveLocM prf f da
solveLocM (STpi2 prf) f (DPair da db)
  = DPair da <$> solveLocM prf f db
solveLocM (STleft prf) f (DInL da)
  = DInL <$> solveLocM prf f da
solveLocM (STright prf) f (DInR db)
  = DInR <$> solveLocM prf f db
solveLocM (STfix prf) f (DmuIns x d)
  = DmuIns x <$> solveLocM (STfix prf) f d
solveLocM (STfix prf) f (DmuDel x d)
  = DmuDel x <$> solveLocM (STfix prf) f d
solveLocM (STfix prf) f (DmuCpy x d)
  = DmuCpy x <$> solveLocM (STfix prf) f d
solveLocM (STfix prf) f (DmuDwn x d)
  = DmuDwn <$> solveLocM prf f x <*> solveLocM (STfix prf) f d
solveLocM _ _ x = return x

solveLoc :: (a :>: b) 
          -> (r b -> Maybe (Patch b)) 
          -> D r a 
          -> D r a
solveLoc prf f = runIdentity . solveLocM prf (return . f)

solveLocWithCtxM :: (Monad m)
                 => (a :>: b)
                 -> (r b -> [r b] -> m (Maybe (Patch b)))
                 -> D r a
                 -> m (D r a)
solveLocWithCtxM prf solver da
  = let
    ctx = conflictsAt prf da
  in runReaderT (solveLocM prf (ReaderT . solver) da) ctx
  
solveLocWithCtx :: (a :>: b)
                -> (r b -> [r b] -> Maybe (Patch b))
                -> D r a
                -> D r a
solveLocWithCtx prf f
  = runIdentity . solveLocWithCtxM prf (\c ctx -> return (f c ctx))

{-
  TODO:
    
    Having our "subtype" datatype (:>:), we want to
    state different conflict solving strategies
    for differnt types!
    
    For instance:
    
solveAt :: (a :>: b) -> (r b -> D Void b) -> D r a -> D r a
-}
