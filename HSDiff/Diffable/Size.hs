{-# OPTIONS_GHC -cpp              #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE PolyKinds            #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE StandaloneDeriving #-}
module HSDiff.Diffable.Size where

import Data.Proxy
#define P Proxy :: Proxy

import HSDiff.Algebra
import HSDiff.Util.FuncComb

class Sized a where
  size :: a -> Int
  
instance {-# OVERLAPPING #-} Sized () where
  size _ = 1
  
instance (Sized (a :: *) , Sized (b :: *)) => Sized (a , b) where
  size (a , b) = size a + size b
  
instance (Sized (a :: *) , Sized (b :: *)) => Sized (Either a b) where
  size = (1 +) . either size size
  
instance {-# OVERLAPPABLE #-} (HasSOP (k :: *) , Sized (SOP k)) => Sized (k :: *) where
  size = size . go
  
instance (HasAlg k , Sized (k ())) => Sized (Fix k) where
  size el = size (hd el) + sum (map size $ ch el)
