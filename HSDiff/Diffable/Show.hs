{-# OPTIONS_GHC -cpp              #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GADTs #-}
module HSDiff.Diffable.Show where

import Data.Proxy
#define P Proxy :: Proxy

-- for when we want to show datatypes:
import Data.Data (dataTypeOf, dataTypeName)

import Text.PrettyPrint
import HSDiff.Algebra
import HSDiff.Diffable.Base
import HSDiff.Util.FuncComb

class PP a where
  pp :: Int -> a -> Doc

--------------------------------------------------
-- House keeping

t :: String -> Doc
t = text

ts :: (Show a) => a -> Doc
ts = t . show

flag :: Doc -> Doc
flag d = t "!" <> d <> t "!"

parensI :: Int -> Doc -> Doc
parensI 0 = id
parensI _ = parens

dec :: Int -> Int
dec x | x <= 0    = 0
      | otherwise = x - 1

set :: Doc -> Doc -> Doc
set x y = x <+> t "=>" <+> y

set3 :: Doc -> Doc -> Doc -> Doc
set3 x y z = x $+$ (t "=>" <+> y) $+$ (t "<=" <+> z)

instance (PP (r ())) => PP (D r ()) where
  pp p (DA ra) = flag (pp 0 ra)
  pp p D1      = t "D1"
  
instance (PP (r (a , b)) , PP (D r a) , PP (D r b)) 
    => PP (D r (a , b)) where
  pp p (DA ra)       = flag (pp 0 ra)
  pp p (DPair da db) = parensI p $ pp (dec p) da 
                   <+> comma 
                   <+> pp (dec p) db
  
instance (PP (r (Either a b)) , Show a , Show b , PP (D r a) , PP (D r b)) 
    => PP (D r (Either a b)) where
  pp p (DInL da)   = parensI p $ t "L" <+> pp (p + 1) da
  pp p (DInR db)   = parensI p $ t "R" <+> pp (p + 1) db
  pp p (DSetL x y) = t "sL" <+> parensI p (set (ts x) (ts y))
  pp p (DSetR y x) = t "sR" <+> parensI p (set (ts y) (ts x))
  pp p (DA ra)       = flag (pp 0 ra)
  
instance {-# OVERLAPPABLE #-} (PP (r a) , HasSOP a , PP (D r (SOP a))) 
    => PP (D r a) where
  pp p (DA ra)       = flag (pp 0 ra)
  pp p (DTag da) = parensI p
                 $ t "D" <+> parens (t "SOP" <+> t (typeStr (P a)))
                 $+$ nest 2 (pp 0 da)
  
instance (PP (r (Fix a)), Show (a ()), PP (D r (a ()))) 
    => PP (D r (Fix a)) where
  pp p (DmuA ra d)  = flag (pp 0 ra) $+$ pp p d
  pp p (DmuEnd)     = t "###"
  pp p (DmuIns x d) = t "+++" <+> parensI p (t $ str x) $+$ pp p d
  pp p (DmuDel x d) = t "---" <+> parensI p (t $ str x) $+$ pp p d
  pp p (DmuCpy x d) = t "===" <+> parensI p (t $ str x) $+$ pp p d
  pp p (DmuDwn x d) = t ">>>" 
                  $+$ nest 2 (pp 0 x)
                  $+$ t "<<<"
                  $+$ pp p d
                  
                  
instance (Show a , PP (r (Atom a))) => PP (D r (Atom a)) where
  pp p (DA ra)     = flag (pp 0 ra)
  pp p D0Cpy       = t "CPY"
  pp p (D0Set x y) = t "A" <+> parens (set (ts x) (ts y))
  
instance PP (Void2 x) where
  pp _ _ = undefined
  
instance {-# OVERLAPPABLE #-} (Show a) => PP (Conflict a) where
  pp _ (UpdUpd o x y) = t "UU" <+> set3 (ts x) (ts o) (ts y)
  pp _ _              = t "Something very wrong happened"
  
instance (Show (a ())) => PP (Conflict (Fix a)) where
  pp _ (UpdUpd o x y) = t "Something very wrong happened"
  pp _ (UpdDel x y)   = t "UD" <+> parens (ts x <+> comma <+> ts y)
  pp _ (DelUpd x y)   = t "DU" <+> parens (ts x <+> comma <+> ts y)
  pp _ (GrowLR x y)   = t "GLR" <+> parens (ts x <+> comma <+> ts y)
  pp _ (GrowL x)      = t "GL" <+> parens (ts x)
  pp _ (GrowR x)      = t "GR" <+> parens (ts x)
  
instance (PP (D r a)) => Show (D r a) where
  show = show . pp 0
  
instance (Show a , PP (Conflict a)) => Show (Conflict a) where
  show = show . pp 0
  
  
