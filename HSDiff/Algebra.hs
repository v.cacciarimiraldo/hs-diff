{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE StandaloneDeriving #-}
module HSDiff.Algebra where

import Data.Proxy

class HasSOP a  where
  type SOP a :: *
  
  go :: a -> SOP a
  og :: SOP a -> a
  
  typeStr :: Proxy a -> String
  
newtype Fix a = Fix { unFix :: a (Fix a) } 

proxyUnfix :: Proxy (Fix a) -> Proxy (a ())
proxyUnfix _ = Proxy
  
class (Eq (a ())) => HasAlg (a :: * -> *) where   
  ar :: Fix a -> Int
  ar = length . ch
  
  ch :: Fix a -> [Fix a]
  hd :: Fix a -> a ()
  
  close :: (a () , [Fix a]) -> Maybe (Fix a , [Fix a])
  
  str :: a () -> String
  str _ = "CONSTRUCTOR"
  
  -- thm:
  --   close (x , l) == Just k , if length l == arHd x, for some k.
  
serialize :: (HasAlg a) => Fix a -> [a ()]
serialize x = hd x : concatMap serialize (ch x)
  
instance (Eq (a ()) , HasAlg a) => Eq (Fix a) where
  a == b = hd a == hd b && all id (map (uncurry (==)) $ zip (ch a) (ch b))
  
instance (Ord (a ()) , HasAlg a) => Ord (Fix a) where
  compare a b 
    = case compare (hd a) (hd b) of
        LT -> LT
        GT -> GT
        EQ -> compareCH (ch a) (ch b)
    where
      compareCH [] [] = EQ
      compareCH [] _  = GT
      compareCH _ []  = LT
      compareCH (x:xs) (y:ys)
        = case compare x y of
            EQ -> compareCH xs ys
            res -> res

instance (Show (a ()) , HasAlg a) => Show (Fix a) where
  show x
    = let hdX = hd x
          chX = ch x
       in str hdX ++ " (" ++ unwords (map show chX) ++ ")"
