{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module HSDiff.Examples.CSV where

import Data.Proxy
import Data.Maybe

import HSDiff.Algebra
import HSDiff.Algebra.List
import HSDiff.Diffable
import HSDiff.Diffable.Show
import HSDiff.Diffable.Traversals
import Data.List (break , intersperse, concat)


-- They correspond exactly to Haskell lists!
fromListDeep :: (a -> b) -> List a -> [b]
fromListDeep f (Fix Nil)         = []
fromListDeep f (Fix (Cons x xs)) = f x : fromListDeep f xs

toListDeep :: (a -> b) -> [a] -> List b
toListDeep _ []     = Fix Nil
toListDeep f (x:xs) = Fix (Cons (f x) $ toListDeep f xs)

-- A CSV file, therefore, is a list of list of atomic strings.
type CSV = List (List (Atom String))

csvProxy :: Proxy CSV
csvProxy = Proxy

-- Now we define a simple way of parsing CSV files encoded in strings.
splitAtAll :: (Eq a) => a -> [a] -> [[a]]
splitAtAll x [] = []
splitAtAll x l
  = let (a1 , rest) = break (== x) l
    in case rest of
        []    -> [a1]
        (_:r) -> a1 : splitAtAll x r
    
csvParseStr :: String -> [[Atom String]]
csvParseStr = map (map Atom . splitAtAll ',') . lines

csvParse :: String -> IO [[Atom String]]
csvParse filepath = csvParseStr <$> readFile filepath

csvParseList :: String -> CSV
csvParseList = toListDeep (toListDeep id) . csvParseStr
    
csvPrint :: CSV -> String
csvPrint = unlines . map (concat . intersperse "," . map unAtom)
         . fromListDeep (fromListDeep id)

------------------------------------------------------------
-- Testing it out!

csvA , csvB , csvO , csvC :: String
csvA = "items,qty,unit\n\
       \flour,2,cp\n\
       \eggs,2,units"

csvO = "items,qty,unit\n\
       \flour,1,cp\n\
       \eggs,2,units"

csvB = "items,qty,unit\n\
       \flour,1,cp\n\
       \eggs,2,unit\n\
       \sugar,2,tsp"
       
csvC = "market,items,qty,unit\n\
       \none,flour,1,cp\n\
       \none,eggs,2,units"
       
mydiff csvA csvB
  = let xa = csvParseList csvA
        xb = csvParseList csvB
    in diff xa xb
    
residualFromJust :: Maybe a -> a
residualFromJust (Just a) = a
residualFromJust _ 
  = error "Conflicting Edits somewhere..."
  
applyFromJust :: Maybe a -> a
applyFromJust (Just a) = a
applyFromJust _
  = error "Non-aligned edit..."
  
--------------------------------
-- CSV's can grow freely

freeGrow :: Conflict a -> Maybe (Patch a)
freeGrow (GrowL x) = Just $ DmuIns x DmuEnd
freeGrow (GrowR y) = Just $ DmuCpy y DmuEnd
freeGrow rest      = Nothing

csvGrow :: PatchC CSV -> Maybe (Patch CSV)
csvGrow = resolved . solveP freeGrow
  
doResiduals csvA csvO csvB
  = let
    xa = csvParseList csvA
    xb = csvParseList csvB
    xo = csvParseList csvO
    dOA = diff xo xa
    dOB = diff xo xb
  in do
    dAC <- res dOB dOA
    dBC <- res dOA dOB
    dAC2 <- csvGrow dAC
    dBC2 <- csvGrow dBC
    return (dAC2 , dBC2)
    
doMerge csvA csvO csvB
  = let
      xa = csvParseList csvA
      xb = csvParseList csvB
      xo = csvParseList csvO
    in do
      (dac , dbc) <- doResiduals csvA csvO csvB
      resA <- apply dac xa
      resB <- apply dbc xb
      if resA == resB
        then return $ "OK\n" ++ (csvPrint resA)
        else return $ "BAD\n" ++ (csvPrint resA) 
                    ++ "######\n"
                    ++ csvPrint resB
     

