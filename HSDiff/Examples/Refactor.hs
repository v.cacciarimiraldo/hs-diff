{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}
module HSDiff.Examples.Refactor where

import HSDiff.Algebra
import HSDiff.Algebra.List
import HSDiff.Diffable
import HSDiff.Diffable.Show
import HSDiff.Diffable.Traversals

import Control.Monad.State

type T    = List (List (Atom Int))
type SubT = List (Atom Int)

l0 , lp , lq :: T
l0 = fromList [ fromList [Atom 1 , Atom 2] , fromList [Atom 3] ]
lp = fromList [ fromList [Atom 2] , fromList [Atom 3 , Atom 1] ]
lq = fromList [ fromList [Atom 4 , Atom 2] , fromList [Atom 3] ]

lres = fromList [ fromList [Atom 2] , fromList [Atom 3 , Atom 4] ]

top = diff l0 lp
toq = diff l0 lq

pullp = diff lp lres
pullq = diff lq lres

respq = let (Just x) = res top toq in x
resqp = let (Just x) = res toq top in x

refactorLoc :: T :>: SubT
refactorLoc = STfix (STtag (STright (STpi1 STrefl)))

refactorSolver :: Conflict SubT 
               -> [Conflict SubT]
               -> Maybe (Patch SubT)
refactorSolver c ctx = aux c ctx
  where
    aux :: Conflict SubT -> [Conflict SubT] -> Maybe (Patch SubT)
    aux c@(GrowL a) ctx
      | Just a' <- fetchDel a ctx 
        = Just $ DmuIns a' DmuEnd
      | otherwise                 = Nothing
    aux c@(DelUpd a b) ctx
      | (GrowL a) `elem` ctx = Just $ DmuDel b DmuEnd
      | otherwise            = Nothing      
    aux c@(GrowR a) ctx
      | Just a' <- fetchDel a ctx 
        = Just $ DmuDwn (diff a a') DmuEnd
      | otherwise                 = Nothing
    aux c@(UpdDel a b) ctx
      | (GrowR b) `elem` ctx = Just DmuEnd
      | otherwise            = Nothing
      
    fetchDel :: (L (Atom Int) ()) 
             -> [Conflict SubT] -> Maybe (L (Atom Int) ())
    fetchDel la [] = Nothing
    fetchDel la (DelUpd x y : d)
      | la == x   = Just y
      | otherwise = fetchDel la d
    fetchDel la (UpdDel y x : d)
      | la == x   = Just y
      | otherwise = fetchDel la d
    fetchDel la (_:d) = fetchDel la d
 
allowRefactor = solveLocWithCtx refactorLoc refactorSolver

danger y = let (Just x) = resolved $ allowRefactor y in x

-------------------------
-- THROWAWAY

k :: [[Int]] -> T
k = fromList . map (fromList . map Atom)

k1 , k2 :: T
k1 = k [[1..10] , [3,5,6] , [7..25] , [34 , 2]]
k2 = k [[1..3] ++ [7..25] , [5] , [34 , 2 , 2 , 2]]
