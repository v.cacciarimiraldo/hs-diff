{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeFamilies #-}
module HSDiff.Examples.SCalc where

import Text.Parsec.Prim hiding (State)
import qualified Text.Parsec.Token as Token
import Text.Parsec.Language (haskellDef)
import Text.Parsec.Expr

import HSDiff.Diffable
import HSDiff.Algebra
import HSDiff.Algebra.List
import HSDiff.TH.GenSOP

type AInt = Atom Int
type ADouble = Atom Double
type AString = Atom String

data Module
  = Mod AString (List Def) Expr
  deriving (Eq , Show)
  
data Def
  = FunDef AInt AString Expr 
  | ValDef AString Expr
  deriving (Eq , Show)

data ExprD
  = LetInD (List Def) ExprD
  | OpD OP ExprD ExprD
  | ValD ADouble
  | VarD AInt
  | SymD AString
  | AppD ExprD ExprD
  deriving (Eq, Show)

newtype ExprF x 
  = ExprF { unExprF
  :: Either (List Def, x) (
      Either (OP , (x , x)) (
        Either ADouble (
          Either AInt (
            Either AString (x , x)))))
  } deriving (Eq , Show)
  
pattern LetIn ld x = ExprF (Left (ld , x))
pattern Op op x1 x2 = ExprF (Right (Left (op , (x1 , x2))))
pattern Val d = ExprF (Right (Right (Left d)))
pattern Var i = ExprF (Right (Right (Right (Left i))))
pattern Sym s = ExprF (Right (Right (Right (Right (Left s)))))
pattern App x y = ExprF (Right (Right (Right (Right (Right (x , y))))))

toExprF :: ExprD -> Fix ExprF
toExprF (LetInD ld e) = Fix (LetIn ld (toExprF e))
toExprF (OpD o x y) = Fix (Op o (toExprF x) (toExprF y))
toExprF (ValD d) = Fix (Val d)
toExprF (VarD i) = Fix (Var i)
toExprF (SymD s) = Fix (Sym s)
toExprF (AppD x y) = Fix (App (toExprF x) (toExprF y))

  
instance HasSOP (ExprF x) where
  type SOP (ExprF x)
    = Either (List Def, x) (
        Either (OP , (x , x)) (
          Either ADouble (
            Either AInt (
              Either AString (x , x)))))

  go = unExprF
  og = ExprF
  
  typeStr _ = "Expr"
  
instance HasAlg ExprF where
  ch (Fix (LetIn ld x)) = [x]
  ch (Fix (Op op x1 x2)) = [x1 , x2]
  ch (Fix (App x y)) = [x , y]
  ch _ = []
  
  hd (Fix (LetIn ld _)) = LetIn ld ()
  hd (Fix (Op op _ _))  = Op op () ()
  hd (Fix (Val d)) = Val d
  hd (Fix (Var i)) = Var i
  hd (Fix (Sym s)) = Sym s
  hd (Fix (App x y)) = App () ()
  
  close (LetIn ld _ , (x:xs)) = Just (Fix (LetIn ld x) , xs)
  close (Op op _ _ , (x:y:xs)) = Just (Fix (Op op x y) , xs)
  close (App _ _ , (x:y:xs)) = Just (Fix (App x y) , xs)
  close (Val d , l) = Just (Fix (Val d) , l)
  close (Var i , l) = Just (Fix (Var i) , l)
  close (Sym s , l) = Just (Fix (Sym s) , l)
  close _ = Nothing
  
  str (LetIn _ _) = "LetIn"
  str (Op op _ _)  = "Op " ++ show op
  str (App _ _) = "App"
  str (Val d) = "Val " ++ show d
  str (Var i) = "Var " ++ show i
  str (Sym s) = "Sym " ++ show s
  
  
data OP
  = ADD | SUB | MUL | DIV
  deriving (Eq , Show)
  
-----------------------------------------------
-- Here we generate the SOP's we need

type Expr = Fix ExprF

econv :: ExprD -> Expr
econv = toExprF
  
$(genSOP ''Module)
$(genSOP ''Def)
$(genSOP ''ExprD)
$(genSOP ''OP)

-----------------------------------------------
-- Parsing os SCalc modules

type Parser = Parsec String ()

myLangDef = haskellDef
  { Token.reservedOpNames = ["+" , "-" , "*" , "/" , "=" , "!" , ";" , "."]
  , Token.reservedNames = ["module", "func" , "is" , "let" , "in"]
  }
  
myLang = Token.makeTokenParser myLangDef

pIden = Atom <$> Token.identifier myLang
pInt  = fromInteger <$> Token.integer myLang
pRes  = Token.reserved myLang
pOp   = Token.reserved myLang
pDouble = (try (Token.float myLang) 
               <|> (fromInteger . toInteger <$> pInt))
parens = Token.parens myLang

pAInt = Atom <$> pInt
pADouble = Atom <$> pDouble

pModule :: Parser Module
pModule = Mod <$> (pRes "module" >> pIden) 
              <*> (fromList <$> many pDef) 
              <*> (pOp "." >> econv <$> pExpr)

pDef = (try pFunDef <|> pValDef) <* pOp ";"

pFunDef :: Parser Def
pFunDef = FunDef <$> (pRes "func" >> pAInt) 
                 <*> (pIden <* pOp "=") 
                 <*> (econv <$> pExpr)

pValDef :: Parser Def
pValDef = ValDef <$> (pIden <* pOp "=") 
                 <*> (econv <$> pExpr)

pExprAtom :: Parser ExprD
pExprAtom = try (ValD <$> pADouble)
        <|> try (SymD <$> pIden)
        <|> try (parens pExpr)
        <|> (VarD <$> (pOp "!" >> pAInt))
        
pExprOp :: Parser ExprD
pExprOp = buildExpressionParser table pExprAtom
  where
    binary  name fun assoc 
      = Infix (do{ pOp name; return fun }) assoc
    table = [[binary "*" (OpD MUL) AssocLeft , binary "/" (OpD DIV) AssocLeft]
            ,[binary "+" (OpD ADD) AssocLeft , binary "-" (OpD SUB) AssocLeft]
            ]

pExpr :: Parser ExprD
pExpr = try pExprLet <|> try pExprApp <|> pExprOp
  where
    pExprLet = LetInD <$> (pRes "let" >> fromList <$> many pDef) <*> (pRes "in" >> pExpr)
    pExprApp = AppD <$> pExprOp <*> pExpr  
    
modParse :: String -> Module
modParse = either (error . show) id . runParser pModule () "<>"

expParse :: String -> Expr
expParse = either (error . show) id . runParser (econv <$> pExpr) () "<>"

----------------------------------------------------------
-- Some examples

module1 :: String
module1 = "\
  \module Test \
  \func 1 f = ! 1 + 4 ; \
  \func 1 g = 2 * ! 1 ; \
  \. g (f 4)"
  
module2 :: String
module2 = "\
  \module Test \
  \func 1 f = ! 1 + 4 ; \
  \. let func 1 g = 2 * ! 1 ; in g (f 4)"
  
module3 :: String
module3  = "\
  \module Test \
  \func 1 f = ! 1 + 4 ; \
  \func 1 g = 3 * ! 1 ; \
  \. g (f 5)"
  
m1 = modParse module1
m2 = modParse module2
m3 = modParse module3

d12 = diff m1 m2
d13 = diff m1 m3
