{-# OPTIONS_GHC -cpp              #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE PolyKinds            #-}
{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE BangPatterns         #-}
{-# LANGUAGE Rank2Types           #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-| This re-exports the main instances
    of the 'Diffable' class, defined in "HSDiff.Diffable.Base",
    and implements some generic functionality over patches.
-}
module HSDiff.Diffable
  ( module HSDiff.Diffable.Base
  , module HSDiff.Diffable.Instances.Coproduct
  , module HSDiff.Diffable.Instances.Product
  , module HSDiff.Diffable.Instances.Fixpoint
  , module HSDiff.Diffable.Instances.SOP
  , Ex(..)
  , resolved
  , djoin
  , forget
  , conflictsAt
  ) where

import Data.Proxy
#define P Proxy :: Proxy

import HSDiff.Diffable.Base
import HSDiff.Diffable.Instances.Coproduct
import HSDiff.Diffable.Instances.Product
import HSDiff.Diffable.Instances.Fixpoint
import HSDiff.Diffable.Instances.SOP
import HSDiff.Util.FuncComb
import HSDiff.Algebra


-- |Can decide whether or not a patch still has conflicts.
resolved :: D r a -> Maybe (D Void2 a)
resolved (DA dra)     = Nothing
resolved (DmuA dra d) = Nothing
resolved D1           = return D1
resolved D0Cpy        = return D0Cpy
resolved (D0Set x y)  = return (D0Set x y)
resolved (DInL da)    = DInL <$> resolved da
resolved (DInR db)    = DInR <$> resolved db
resolved (DSetL x y)  = return (DSetL x y)
resolved (DSetR x y)  = return (DSetR x y)
resolved (DTag d)     = DTag <$> resolved d
resolved (DPair da db) = DPair <$> resolved da <*> resolved db
resolved (DmuIns x d) = DmuIns x <$> resolved d
resolved (DmuDel x d) = DmuDel x <$> resolved d
resolved (DmuCpy x d) = DmuCpy x <$> resolved d
resolved (DmuDwn x d) = DmuDwn <$> resolved x <*> resolved d 
resolved DmuEnd       = Just DmuEnd


-- |The free-monad structure gives us a multiplication for free.
djoin :: D (D r) a -> D r a
djoin D1          = D1
djoin D0Cpy       = D0Cpy
djoin (D0Set x y) = D0Set x y
djoin (DInL da)   = DInL (djoin da)
djoin (DInR db)   = DInR (djoin db)
djoin (DSetL x y) = DSetL x y
djoin (DSetR x y) = DSetR x y
djoin (DTag d)    = DTag (djoin d)
djoin (DPair da db) = DPair (djoin da) (djoin db)
djoin (DmuIns x d) = DmuIns x (djoin d)
djoin (DmuDel x d) = DmuDel x (djoin d)
djoin (DmuCpy x d) = DmuCpy x (djoin d)
djoin (DmuDwn x d) = DmuDwn (djoin x) (djoin d)
djoin DmuEnd       = DmuEnd
djoin (DA dra) = dra
djoin (DmuA dra d) = dmucat dra (djoin d)
  where
    dmucat :: (HasAlg a) => D r (Fix a) -> D r (Fix a) -> D r (Fix a)
    dmucat DmuEnd        d2 = d2
    dmucat (DmuIns x d1) d2 = DmuIns x (dmucat d1 d2)
    dmucat (DmuDel x d1) d2 = DmuDel x (dmucat d1 d2)
    dmucat (DmuCpy x d1) d2 = DmuCpy x (dmucat d1 d2)
    dmucat (DmuDwn x d1) d2 = DmuDwn x (dmucat d1 d2)
    dmucat (DmuA ra d1)  d2 = DmuA ra  (dmucat d1 d2)
    
-- |And our forgetful functor. This one is not very usefull, though.
forget :: D r a -> [Ex r a]
forget (DInL da) = map (exmap STleft)  $ forget da
forget (DInR db) = map (exmap STright) $ forget db
forget (DTag da) = map (exmap STtag) $ forget da
forget (DPair da db) 
  =  map (exmap STpi1) (forget da) 
  ++ map (exmap STpi2) (forget db)
forget (DmuIns _ d)  = forget d
forget (DmuDel _ d)  = forget d
forget (DmuCpy _ d)  = forget d
forget (DmuDwn dx d) = map (exmap STfix) (forget dx) ++ forget d
forget (DmuA ra d)   = Ex STrefl ra : forget d
forget (DA ra)       = Ex STrefl ra : []
forget _             = []

-- |Here is a much more usefull version!
conflictsAt :: b :>: a -> D r b -> [r a]
conflictsAt STrefl (DA ra)            = [ra]
conflictsAt STrefl (DmuA r d)         = r : (conflictsAt STrefl d)
conflictsAt STrefl (DmuIns _ d)       = conflictsAt STrefl d
conflictsAt STrefl (DmuDwn _ d)       = conflictsAt STrefl d
conflictsAt STrefl (DmuCpy _ d)       = conflictsAt STrefl d
conflictsAt STrefl (DmuDel _ d)       = conflictsAt STrefl d
conflictsAt (STtag prf) (DTag da)     = conflictsAt prf da
conflictsAt (STpi1 prf) (DPair da db) = conflictsAt prf da
conflictsAt (STpi2 prf) (DPair da db) = conflictsAt prf db
conflictsAt (STleft prf) (DInL da)    = conflictsAt prf da
conflictsAt (STright prf) (DInR da)   = conflictsAt prf da
conflictsAt (STfix prf) (DmuIns _ d)  = conflictsAt (STfix prf) d
conflictsAt (STfix prf) (DmuDel _ d)  = conflictsAt (STfix prf) d
conflictsAt (STfix prf) (DmuCpy _ d)  = conflictsAt (STfix prf) d
conflictsAt (STfix prf) (DmuDwn dx d)  
  = conflictsAt prf dx ++ conflictsAt (STfix prf) d
conflictsAt (STfix prf) (DmuA ra d)
  = conflictsAt (STfix prf) d
conflictsAt _ _ = []
    
